provider "aws"                          {
 region                                 =         "us-east-1"
 shared_credentials_file                =         "/nuvym/aws/.cred"
 profile                                =         "default"
}

resource "aws_instance" "amazon-inst"       {
 ami                                    =         "ami-0c2b8ca1dad447f8a"
 instance_type                          =         "t3.micro"
 vpc_security_group_ids                 =         ["sg-09ac1ad2459effba9"]
 subnet_id                              =         "subnet-0dcf22869d0faa3bb"
 associate_public_ip_address            =         "1"
# iam_instance_profile                   =        "ecsInstanceRole" 
# cpu_core_count                         =        "1" 
 cpu_threads_per_core                   =         "2" 
 instance_initiated_shutdown_behavior   =         "stop"
 disable_api_termination                =         "0"
 monitoring                             =         "1" 
 tenancy                                =         "default"

root_block_device                       { 
 volume_size                            =         "10"
 delete_on_termination                  =         "true" 
 encrypted                              =         "true" 
# kms_key_id                            = 
}

tags                                    =         {
 Name                                   =         "amazon-training"
} 

volume_tags                             = {
 Name                                   =         "amazon-training-vol"
}

provisioner "file"                      {
 source                                 =         "/nuvym/training/scripts/init.sh"
 destination                            =         "/home/ec2-user/init.sh"
}

provisioner "remote-exec"               {
 inline                                 = [
 "chmod +x /home/ec2-user/init.sh",
 "/home/ec2-user/init.sh",
 "rm /home/ec2-user/init.sh",
 ]
}

availability_zone                       =        "us-east-1a"

key_name                                =        "training" 

connection                              {
 type                                   =        "ssh"
 user                                   =        "ec2-user"
 private_key                            =        "${file("/nuvym/aws/training.pem")}"
 host                                   =         self.public_ip
 port                                   =        "22"
}

}
